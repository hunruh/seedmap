package com.seedmap;

import java.io.File;
import java.util.Random;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.server.Sizeable;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;

public class SeedmapController extends SeedmapView implements View {
	public SeedmapController() {
		super();

		generateTable();
		
		registerButton.setCaption("Register a plot");
		registerButton.setWidth("100%");

		FileResource resource = new FileResource(new File("/Users/hunruh/Code/SeedMap/WebContent/VAADIN/themes/seedmap/imgs/map.jpg"));
		map.setSource(resource);
		map.setHeight("100%");
	}
	
	private void generateTable() {
		//generate the table
		String[] registrationOps = new String[]{"Open", "Reserved"};
		Random rand = new Random();
		plotTable.addContainerProperty("Plot Num", Integer.class, null);
		plotTable.addContainerProperty("Status",  String.class, null);
		plotTable.addContainerProperty("Name",  String.class, null);
		plotTable.addContainerProperty("Email",  String.class, null);
		
		for(int i = 0; i < 100; i++) {
			String name = "";
			String email = "";
			int open = rand.nextInt(2);
			if(open == 1) {
				name = "John Doe";
				email = "john.doe@gmail.com";
			}
			
			plotTable.addItem(new Object[]{i + 1, registrationOps[open], name, email}, i);
		}
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
}
